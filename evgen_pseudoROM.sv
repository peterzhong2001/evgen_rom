// Simulation ROM for testing the event generator module. Read only and combinational.
// Memory content is loaded through a text file named "memcontent.txt". This text file
// can be generated from the python script.

`timescale 1ns/1ps
`define MEMFILE "memcontent.txt"

module evgen_pseudoROM #(SIZE = 1024, WIDTH = 32) (rdAddr, rdData);
    input  logic [$clog2(SIZE)-1 : 0] rdAddr;
    output logic [WIDTH - 1:0]        rdData;

    // memory array
    logic [WIDTH - 1:0] mem [SIZE - 1 : 0];

    // initialization procedure
    initial begin
        $readmemb(MEMFILE, mem);
    end

    assign rdData = (rdAddr < SIZE) ? mem[rdAddr] : 'x;
endmodule

module evgen_pseudoROM_testbench();
    logic [ 9:0] rdAddr;
    logic [31:0] rdData;
    
    evgen_pseudoROM dut (.*);
    
    integer i;
    initial begin
        for (i = 0; i < 10; i=i+1) begin
            rdAddr = i; #50;
        end
    end
endmodule