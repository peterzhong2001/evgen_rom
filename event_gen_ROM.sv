// This is a ROM-based event generator. It uses a 32 bit x 1024 words ROM.
// Each word in the ROM represents 2 16-bit quarter-row hitmaps.
// While active, the event generator outputs 1 qrow, ccol, and hitmap data each cycle.
// EOS behavior: if ccol goes out of bound, or if number of hits reaches hits per stream, 
//               the event generator toggles dout_eos high and terminates the stream.

// Assumptions about data in ROM:
// 1. There is no 'x in ROM 
// 2. There is no '0 in ROM 

// EOE behaviors are not implemented yet

`timescale 1ns / 1ps

`include "defines_rd53b.sv"

module event_gen_ROM
(
    input   logic           clk             ,  
    input   logic           reset           , 

    input   logic           start           , // Begin stream generation if enabled
    input   logic           ready           , // Downstream block can accept new hit data
    output  logic           busy            , // Set low after end of a stream if enable is low.   
    
    input   logic   [31:0]  seed            , // Seed to initialize the pseudo-random number generator
    input   logic   [15:0]  hits_per_stream , // Set how many hits in a stream before setting start
    input   logic           debug,            // Set to use incremented address to read ROM

    output  logic           dout_dv         , //  Set when new dout_hit produced  
    output  logic           dout_eos        , //  Set for end-of-stream. Last hit in stream 
    output  logic           dout_eoe        , //  Set for end-of-event. Last hit in an event 
    output  logic   [ 7:0]  dout_hit_qrow   , // 
    output  logic   [ 5:0]  dout_hit_ccol   , // 
    output  logic   [15:0]  dout_hit_hitmap   // 
);

    logic [31:0] random_dout;                 // LFSR output
    logic [31:0] rom_out;                     // ROM output of 32-bit hitmap
    
    // Divide rom_out into two separate hitmaps
    logic [15:0] rom_out_upper, rom_out_lower;
    assign rom_out_upper = rom_out[31:16];
    assign rom_out_lower = rom_out[15:0];
    
    //----------------------------------------------------------------------------
    // Address incrementer for debugging
    //----------------------------------------------------------------------------
    logic [9:0] dbg_incr_out;
    always_ff @(posedge clk)
        dbg_incr_out  <= reset ? '0 : dbg_incr_out  + 1;

    //----------------------------------------------------------------------------
    // FSM
    //----------------------------------------------------------------------------
    integer      count_hits;                    // number of hits in the current stream
    logic [15:0] reg_hits_per_stream;           // registering hits per stream when starting pattern generation
    logic [1:0]  select_incr;                   // increment row by 0, 1, 2, or 3
    logic [15:0] lower_hitmap_buffer;           // storing the lower hitmap for outputting in the next cycle
    logic [7:0]  next_qrow;
    logic [5:0]  next_ccol;

    enum {
        S_IDLE,             // Wait for 'start'
        S_HITMAP1,           // Output hitmap
        S_HITMAP2,          // output the 2nd hitmap if there is one
        S_EOS               // Complete a stream
    } state_fsm;

    always_ff @(posedge clk) begin
        if (reset) begin
            state_fsm       <= S_IDLE;
            count_hits      <= 1;

            busy            <= 1'b0;
            dout_dv         <= 1'b0;
            dout_eos        <= 1'b0;
            dout_eoe        <= 1'b0;

            next_qrow       <= '0;
            next_ccol       <= '0;

            dout_hit_qrow   <= 'x;
            dout_hit_ccol   <= 'x;
            dout_hit_hitmap <= 'x;
        end
        else begin
            case (state_fsm)
                S_IDLE: begin
                    if (start) begin
                        busy <= 1'b1;
                        state_fsm <= S_HITMAP1;
                    end
                    else begin
                        busy <= 1'b0;
                    end
                    count_hits          <= 1;
                    dout_dv             <= 1'b0;
                    dout_eos            <= 1'b0;
                    dout_eoe            <= 1'b0;
                    
                    next_qrow           <= '0;
                    next_ccol           <= '0;

                    dout_hit_qrow       <= 8'b0;
                    dout_hit_ccol       <= 6'b0;
                    dout_hit_hitmap     <= 16'b0;
                    reg_hits_per_stream <= hits_per_stream;       // storing hits per stream in designated register on startup
                end
                S_HITMAP1: begin
                    busy <= 1'b1;
                    if (ready) begin
                        dout_dv         <= 1'b1;                  // output is valid
                        count_hits      <= count_hits + 1;
                        dout_eos        <= 1'b0;
                        dout_eoe        <= 1'b0;
                    
                        // hitmap handling
                        if (rom_out_upper == '0) begin                             // upper hitmap is empty, only outputs lower hitmap
                            dout_hit_hitmap <= rom_out_lower;
                            state_fsm <= (count_hits + 1 == reg_hits_per_stream) ? S_EOS : S_HITMAP1;
                            if (next_qrow + 1 == QROW_MAX) begin                      // lower hitmap is out of bound
                                if (next_ccol + 1 == CCOL_MAX) begin                 // if the increment brings EOE, do not increment and output at final valid address
                                    state_fsm <= S_EOS;
                                    dout_hit_qrow  <= next_qrow;
                                    dout_hit_ccol  <= next_ccol;
                                end
                                else begin
                                    dout_hit_qrow  <= 0;
                                    dout_hit_ccol  <= next_ccol + 1;
                                end
                            end
                            else begin
                                dout_hit_qrow <= next_qrow + 1;  // add one to compensate for empty upper hitmap
                                dout_hit_ccol <= next_ccol;
                            end
                            if (next_qrow + 1 + select_incr + 1 >= QROW_MAX) begin
                                if (next_ccol + 1 == CCOL_MAX) state_fsm <= S_EOS;
                                else begin
                                    next_ccol <= next_ccol + 1;
                                    next_qrow <= next_qrow + 1 + select_incr + 1 - QROW_MAX;
                                end
                            end
                            else next_qrow <= next_qrow + 1 + select_incr + 1;           // increment row
                        end
                        else begin                                                 // at least upper hitmap is not empty
                            dout_hit_hitmap <= rom_out_upper;
                            dout_hit_qrow   <= next_qrow;
                            dout_hit_ccol   <= next_ccol;
                            if (rom_out_lower != '0) begin                         // lower hitmap is also not empty. Store in buffer and outputs next cycle
                                state_fsm <= (count_hits + 1 == reg_hits_per_stream) ? S_EOS : S_HITMAP2;
                                lower_hitmap_buffer <= rom_out_lower;
                                if (next_qrow + 1 == QROW_MAX) begin                      // lower hitmap is out of bound
                                    if (next_ccol + 1 == CCOL_MAX) state_fsm <= S_EOS;
                                    else begin
                                        next_qrow  <= 0;
                                        next_ccol  <= next_ccol + 1;
                                    end
                                end
                                else begin
                                    next_qrow <= next_qrow + 1;
                                    next_ccol <= next_ccol;
                                end                        // further handling in S_HITMAP2
                            end
                            else begin
                                state_fsm <= (count_hits + 1 == reg_hits_per_stream) ? S_EOS : S_HITMAP1;            // lower hitmap is empty
                                if (next_qrow + select_incr + 1 >= QROW_MAX) begin
                                    if (next_ccol + 1 == CCOL_MAX) state_fsm <= S_EOS;
                                    else begin
                                        next_qrow <= next_qrow + select_incr + 1 - QROW_MAX;
                                        next_ccol <= next_ccol + 1;
                                    end
                                end
                                else next_qrow <= next_qrow + select_incr + 1;           // increment row
                            end
                        end
                    end
                    else begin
                        dout_dv  <= 1'b0;
                        dout_eos <= 1'b0;
                        dout_eoe <= 1'b0;
                    end
                end
                S_HITMAP2: begin
                    dout_dv         <= 1'b1;                  // output is valid
                    count_hits      <= count_hits + 1;        // increment count
                    dout_hit_qrow   <= next_qrow;
                    dout_hit_ccol   <= next_ccol;
                    dout_hit_hitmap <= lower_hitmap_buffer;
                    
                    // qrow and ccol handling for the next cycle
                    if (next_qrow + select_incr + 1 >= QROW_MAX) begin
                        next_qrow <= next_qrow + select_incr + 1 - QROW_MAX;
                        next_ccol <= next_ccol + 1;
                    end
                    else next_qrow <= next_qrow + select_incr + 1;           // increment row
                    // proceed to EOS if current hit is last hit
                    state_fsm <= (count_hits + 1 == reg_hits_per_stream) ? S_EOS : S_HITMAP1;
                end
                S_EOS: begin                                    // set dv and eos at the same time         
                    dout_dv             <= 1'b1;
                    dout_eos            <= 1'b1;
                    busy                <= 1'b0;
                    next_ccol           <= '0;
                    next_qrow           <= '0;
                    count_hits          <= 1;
                    reg_hits_per_stream <= hits_per_stream;

                    state_fsm           <= (ready) ? S_HITMAP1 : S_IDLE;
                end
                default: begin
                    state_fsm       <= S_IDLE;
                    count_hits      <= 1;
        
                    busy            <= 1'b0;
                    dout_dv         <= 1'b0;
                    dout_eos        <= 1'b0;
                    dout_eoe        <= 1'b0;
        
                    next_qrow       <= '0;
                    next_ccol       <= '0;
        
                    dout_hit_qrow   <= 'x;
                    dout_hit_ccol   <= 'x;
                    dout_hit_hitmap <= 'x;
                end
            endcase
        end
    end

    //----------------------------------------------------------------------------
    // ROM module containing hitmap patterns
    //----------------------------------------------------------------------------
    logic [9:0] rom_rdAddr;
    assign rom_rdAddr = debug ? dbg_incr_out : random_dout[9:0];
    // ----------------------- PseudoROM for testing -----------------------------
    /*
    evgen_pseudoROM rom
    (
        .rdAddr (rom_rdAddr),                // output from LFSR to select pattern
        .rdData (rom_out)                    // ROM output of 32-bit hitmap
    );
    */
    // ----------------------- Xilinx IP ROM -------------------------------------
    rom_evgen rom1
    (
        .clka    (clk),
        .addra (rom_rdAddr),
        .douta   (rom_out)
    );

    //----------------------------------------------------------------------------
    // Random data used for setting cluster and line generator parameters
    //----------------------------------------------------------------------------
    simplePRNG u_simplePRNG       // 32-bit unparametrized LFSR
    (
        .clk        ( clk           ),  // input   logic           
        .reset      ( reset         ),  // input   logic           
        .restart    ( 1'b0          ),  // input   logic           
        .seed       ( seed          ),  // input   logic   [31:0]  
        .step       ( 1'b1          ),  // input   logic           
  
        .dout       ( random_dout   ),  // takes the bottom 10 bits of the LFSR output 
        .new_data   (               )   // output  reg             
    );
    
    assign select_incr = random_dout[1:0];

endmodule

module event_gen_ROM_testbench();
    logic           clk             ;  
    logic           reset           ; 

    logic           start           ;
    logic           ready           ;
    logic           busy            ;   
    
    logic   [31:0]  seed            ;
    logic   [15:0]  hits_per_stream ;
    logic           debug;

    logic           dout_dv         ; 
    logic           dout_eos        ; 
    logic           dout_eoe        ; 
    logic   [ 7:0]  dout_hit_qrow   ; 
    logic   [ 5:0]  dout_hit_ccol   ;  
    logic   [15:0]  dout_hit_hitmap ;

    event_gen_ROM dut (.*);
    
    assign debug = 1'b0;
    assign seed = 32'h1A3B765C;
    assign hits_per_stream = 16'd200;
    assign ready = 1'b1;

    initial begin // Set up the clock
		clk <= 0;
		forever #(50) clk <= ~clk;
	end
	
	initial begin
		reset <= 1; start <= 0;                                                    repeat(2)   @(posedge clk);
		reset <= 0; start <= 0;                                                    repeat(2)   @(posedge clk);
                    start <= 1;                                                    repeat(100) @(posedge clk);
		$stop;
	end
endmodule