"""
This script generates random 32-bit hitmap patterns and stores it in a file.

Available patterns:
    '1bit': one bit set in the 32-bit hitmap
    'rshape': one r-shaped block in the 32-bit hitmap

More patterns can be added by adding subroutine functions to the section named
"Subroutines for typical patterns" below.

Modify parameters of subroutine functions in the main function to control the frequency
of pattern appearance in the memory.
Modify the 'ip_rom' boolean in the main function to set the format of the output file to
either pseudoROM programming format or coe format.
"""

import random

def printHitmap(array):
    """
    Debug function. Takes an array and prints out the 32-bit hitmap represented by the array
    in a 8x4 grid for visual examination.
    """
    print(array[0:8])
    print(array[8:16])
    print(array[16:24])
    print(array[24:32])
    print()

def unpackHitmap(array):
    """
    Takes an array and unpacks it into a string that represents the 32-bit mem content.
    """
    out = ""
    for i in range(0, len(array)):
        out += str(array[i])
    return out

def GenRandomHitmap(file, pattern, iter, ip_rom):
    """
    Takes a file, a pattern string code, and an integer representing iterations. Generates iterations of
    corresponding hit patterns and store the 32-bit string representation into the file. Set ip_rom to true
    if generating coe-format file.
    """
    array_out = [0] * 32
    random.seed()
    for i in range(0, iter):
        position = random.randint(0, 31)
        if (pattern == '1bit'):
            array_out = OneBitSubroutine(position)
        elif (pattern == 'rshape'):
            array_out = RShapeSubroutine(position)
        else: # Handling invalid pattern input
            print('ERROR: INVALID INPUT PATTERN')
        printHitmap(array_out) # debug
        if (ip_rom):
            file.write(unpackHitmap(array_out) + ',\n')
        else:
            file.write(unpackHitmap(array_out) + '\n')

# -------------------------------------------------------------------------------------------------
# ---------------------------- Subroutines for typical patterns -----------------------------------
# -------------------------------------------------------------------------------------------------

def OneBitSubroutine(position):
    """
    Takes a position and returns an array representation of a 1-bit hitmap.
    """
    array_out = [0] * 32
    array_out[position] = 1
    return array_out

def RShapeSubroutine(position):
    """
    Takes a position and returns an array representation of a r-shaped hitmap.
    """
    while position % 8 == 7 or position > 23: # avoid out of bound cases
            position = random.randint(0, 31)
    array_out = [0] * 32
    array_out[position] = 1
    array_out[position + 1] = 1
    array_out[position + 8] = 1
    return array_out

# -------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------
# -------------------------------------------------------------------------------------------------

def main():
    ip_rom = True                                         # set this to true for generating COE; set this to false for generating pseudoROM txt
    if (ip_rom):
        f = open('memcontent.coe', 'w')                   # for loading Xilinx IP ROM
        f.write('memory_initialization_radix=2;\n')
        f.write('memory_initialization_vector=\n')
    else:
        f = open('memcontent.txt', 'w')
    GenRandomHitmap(f, '1bit', 512, ip_rom)
    GenRandomHitmap(f, 'rshape', 512, ip_rom)
    f.close()

if __name__ == '__main__':
    main()